package watttime

import (
	"fmt"
	//"time"
	//"net/url"
)

type FuelsService service

type FuelType struct {
	URL              string     `json:"url"`
	Name             string     `json:"name"`
	Description      string     `json:"description"`
	IsRenewable      bool       `json:"is_renewable"`
	IsFossil         bool       `json:"is_fossil"`
}

func (s *FuelsService) GetFuelTypes() ([]*FuelType, *Response, error) {
	u := fmt.Sprintf("fuels")
	req, err := s.client.NewRequest("GET", u, nil)
	if err != nil {
		return nil, nil, err
	}

	var c []*FuelType
	resp, err := s.client.Do(req, &c)
	if err != nil {
		 return nil, resp, err
	}

	return c, resp, err
}
