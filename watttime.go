/*

WATTIME THINGS

*/

package watttime

import (
	"fmt"
	"io"
	"sort"
	"strings"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/google/go-querystring/query"
)

// Values for this library
const (
	libraryVersion = "1"
	userAgent = "go-watttime" + libraryVersion
	defaultBaseURL = "https://api.watttime.org:443/api/v1/"
	apiDocsURL = "https://api.watttime.org/docs/"
)


type Client struct {
	client *http.Client
	baseURL *url.URL
	userAgent string
	authToken string

	//available services
	Fuels       *FuelsService
}

type service struct {
	client *Client
}

//NewClient ... setup a default Client
func NewClient(httpClient *http.Client, authToken string) *Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	c := &Client{client: httpClient, userAgent: userAgent, authToken: authToken,}
	if err := c.SetBaseURL(defaultBaseURL); err != nil {
		panic(err)
	}

	// setup services
	c.Fuels = &FuelsService{client: c}

	return c
}

//BaseURL ... get the base url as a url
func (c *Client) BaseURL() *url.URL {
	u := *c.baseURL
	return &u
}

// SetBaseURL ... verify and format url
func (c *Client) SetBaseURL(urlStr string) error {
	if !strings.HasSuffix(urlStr, "/") {
		urlStr += "/"
	}

	var err error
	c.baseURL, err = url.Parse(urlStr)
	return err
}

//NewRequest ... make a new request
func (c *Client) NewRequest(method, path string, opt interface{}) (*http.Request, error) {
	u := *c.baseURL
	u.Opaque = c.baseURL.Path + path

	if opt != nil {
		q, err := query.Values(opt)
		if err != nil {
			return nil, err
		}
		u.RawQuery = q.Encode()
	}

	req := &http.Request{
		Method:         method,
		URL:            &u,
		Proto:          "HTTP/1.1",
		ProtoMajor:     1,
		ProtoMinor:     1,
		Header:         make(http.Header),
		Host:           u.Host,
	}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", c.authToken)
	// if c.userAgent != "" {
	// 	req.Header.Set("User-Agent", c.userAgent)
	// }

	fmt.Printf("%s\n", req)
	return req, nil
}

type Response struct {
	*http.Response
}

// NewResponse ... a new respons
func NewResponse(res *http.Response) *Response {
	response := &Response{Response: res}
	return response
}

// Do ... do the request
func (c *Client) Do (req *http.Request, v interface{}) (*Response, error) {
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	response := NewResponse(resp)
	err = CheckResponse(resp)

	if err != nil {
		return response, err
	}

	if v != nil {
		if w, ok := v.(io.Writer); ok {
			_, err = io.Copy(w, resp.Body)
		} else {
			err = json.NewDecoder(resp.Body).Decode(v)
		}
	}
	fmt.Printf("%s\n", resp)
	fmt.Printf("%s\n", err)
	return response, err
}

// CheckResponse ... Verify the API response if valid
func CheckResponse(res *http.Response) error {
	switch res.StatusCode {
	case 200, 201, 304:
		return nil
	}

	errorResponse := &ErrorResponse{Response: res}
	data, err := ioutil.ReadAll(res.Body)
	if err == nil && data != nil {
		var raw interface {}
		if err := json.Unmarshal(data, &raw); err != nil {
			errorResponse.ErrMessage = "failed to parse unknown error format"
		}

		errorResponse.ErrMessage = ParseError(raw)
	}

	return errorResponse
}

type ErrorResponse struct {
	Response *http.Response
	ErrMessage string
}

func (e *ErrorResponse) Error() string {
	path, _ := url.QueryUnescape(e.Response.Request.URL.Opaque)
	u := fmt.Sprintf("%s://%s%s", e.Response.Request.URL.Scheme, e.Response.Request.URL.Host, path)
	return fmt.Sprintf("%s %s: %d %s", e.Response.Request.Method, u, e.Response.StatusCode, e.ErrMessage)
}
// ParseError ... Prints out the error nicely
func  ParseError(raw interface{}) string {
	switch raw := raw.(type) {
	case string:
		return raw

	case []interface{}:
		var errs []string
		for _, v := range raw {
			errs = append(errs, ParseError(v))
		}
		return fmt.Sprint("[%s]", strings.Join(errs, ", "))

	case map[string]interface{}:
		var errs []string
		for k, v := range raw {
			errs = append(errs, fmt.Sprintf("{%s: %s}", k, ParseError(v)))
		}
		sort.Strings(errs)
		return strings.Join(errs, ", ")

	default:
		return fmt.Sprintf("unexpected error type: %T", raw)

	}
}
